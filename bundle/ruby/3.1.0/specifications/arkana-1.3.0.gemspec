# -*- encoding: utf-8 -*-
# stub: arkana 1.3.0 ruby lib

Gem::Specification.new do |s|
  s.name = "arkana".freeze
  s.version = "1.3.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "changelog_uri" => "https://github.com/rogerluan/arkana/blob/main/CHANGELOG.md", "homepage_uri" => "https://github.com/rogerluan/arkana", "rubygems_mfa_required" => "true", "source_code_uri" => "https://github.com/rogerluan/arkana" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Roger Oba".freeze]
  s.date = "2022-11-08"
  s.email = ["rogerluan.oba@gmail.com".freeze]
  s.executables = ["arkana".freeze]
  s.files = ["bin/arkana".freeze]
  s.homepage = "https://github.com/rogerluan/arkana".freeze
  s.licenses = ["BSD-2-Clause".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.6.9".freeze)
  s.rubygems_version = "3.3.3".freeze
  s.summary = "Store your keys and secrets away from your source code. Designed for Android and iOS projects.".freeze

  s.installed_by_version = "3.3.3" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<colorize>.freeze, ["~> 0.8"])
    s.add_runtime_dependency(%q<dotenv>.freeze, ["~> 2.7"])
    s.add_runtime_dependency(%q<yaml>.freeze, ["~> 0.2"])
  else
    s.add_dependency(%q<colorize>.freeze, ["~> 0.8"])
    s.add_dependency(%q<dotenv>.freeze, ["~> 2.7"])
    s.add_dependency(%q<yaml>.freeze, ["~> 0.2"])
  end
end
