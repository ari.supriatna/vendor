# frozen_string_literal: true

module DangerSwiftlint
  VERSION = '0.31.0'
  SWIFTLINT_VERSION = '0.50.0'
  SWIFTLINT_HASH = '7ebfe280424851166c4116a360786f38'
end
