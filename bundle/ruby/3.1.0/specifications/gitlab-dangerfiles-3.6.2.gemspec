# -*- encoding: utf-8 -*-
# stub: gitlab-dangerfiles 3.6.2 ruby lib

Gem::Specification.new do |s|
  s.name = "gitlab-dangerfiles".freeze
  s.version = "3.6.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "allowed_push_host" => "https://rubygems.org", "changelog_uri" => "https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles/-/releases", "homepage_uri" => "https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles", "source_code_uri" => "https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["GitLab".freeze]
  s.bindir = "exe".freeze
  s.date = "2022-11-04"
  s.description = "This gem provides common Dangerfile and plugins for GitLab projects.".freeze
  s.email = ["gitlab_rubygems@gitlab.com".freeze]
  s.homepage = "https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5.0".freeze)
  s.rubygems_version = "3.3.3".freeze
  s.summary = "This gem provides common Dangerfile and plugins for GitLab projects.".freeze

  s.installed_by_version = "3.3.3" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<rake>.freeze, [">= 0"])
    s.add_runtime_dependency(%q<danger-gitlab>.freeze, [">= 8.0.0"])
    s.add_runtime_dependency(%q<danger>.freeze, [">= 8.4.5"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_development_dependency(%q<rspec-parameterized>.freeze, [">= 0"])
    s.add_development_dependency(%q<timecop>.freeze, [">= 0"])
    s.add_development_dependency(%q<webmock>.freeze, [">= 0"])
    s.add_development_dependency(%q<climate_control>.freeze, [">= 0"])
    s.add_development_dependency(%q<rufo>.freeze, [">= 0"])
  else
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<danger-gitlab>.freeze, [">= 8.0.0"])
    s.add_dependency(%q<danger>.freeze, [">= 8.4.5"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
    s.add_dependency(%q<rspec-parameterized>.freeze, [">= 0"])
    s.add_dependency(%q<timecop>.freeze, [">= 0"])
    s.add_dependency(%q<webmock>.freeze, [">= 0"])
    s.add_dependency(%q<climate_control>.freeze, [">= 0"])
    s.add_dependency(%q<rufo>.freeze, [">= 0"])
  end
end
