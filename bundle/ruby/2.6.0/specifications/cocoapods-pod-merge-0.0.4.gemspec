# -*- encoding: utf-8 -*-
# stub: cocoapods-pod-merge 0.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "cocoapods-pod-merge".freeze
  s.version = "0.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Siddharth Gupta".freeze]
  s.date = "2019-11-09"
  s.description = "Cocoapods plugin to merge your pods into one framework, to reduce dylib loading time on app startup.".freeze
  s.email = ["siddharth.gupta@grabtaxi.com".freeze]
  s.homepage = "https://github.com/grab/cocoapods-pod-merge".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0.0".freeze)
  s.rubygems_version = "3.0.3.1".freeze
  s.summary = "Cocoapods plugin to merge your pods into one framework, to reduce dylib loading time on app startup.".freeze

  s.installed_by_version = "3.0.3.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
    else
      s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.3"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
  end
end
