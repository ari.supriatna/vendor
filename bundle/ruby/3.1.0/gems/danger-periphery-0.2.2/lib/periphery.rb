# frozen_string_literal: true

require 'periphery/runner'
require 'periphery/checkstyle_parser'
require 'periphery/json_parser'
